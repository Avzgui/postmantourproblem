CXX = g++
SRC = src/*.cpp
INC = include
LDFLAGS= -lboost_regex -lboost_graph \
		-L/usr/local/lib -lgecodeint -lgecodesearch \
		-lgecodeflatzinc -lgecodefloat -lgecodegist \
		-lgecodekernel -lgecodeset -lgecodesupport \
		-lgecodedriver -lgecodeminimodel
DIRARCH = M2R_IADE3_Antoine_RICHARD_11410951
DIREXEC = bin
EXEC = $(DIREXEC)/postman_tour

all : $(EXEC)

$(EXEC) : $(INC)/* $(SRC)
	mkdir -p $(DIREXEC)
	$(CXX) -std=c++11 -Wall -pedantic $(SRC) -I $(INC) ${LDFLAGS} -o $(EXEC)

clean :
	rm -R $(DIREXEC)

archive :
	mkdir -p $(DIRARCH)
	cp -R src $(DIRARCH)
	cp Makefile $(DIRARCH)
	tar -czvf $(DIRARCH).tar.gz $(DIRARCH)
	rm -R $(DIRARCH)
