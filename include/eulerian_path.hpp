/*!
 * \file eulerian_path.hpp
 * \author Antoine Richard
 */

#ifndef EULERIAN_PATH_HPP
#define EULERIAN_PATH_HPP

#include <iostream>

#include <gecode/int.hh>
#include <gecode/search.hh>

#include <boost_graph.hpp>

/*!
 * \class EulerianPath
 * \brief Constraint search of eulerian path in a undirected graph
 */
class EulerianPath : public Gecode::Space {

	public : 

		EulerianPath(UGraph);

		EulerianPath(bool, EulerianPath&);
        
        virtual Gecode::Space* copy(bool);

        void print(void) const;

        //DGraph toBoostGraph(void) const;

	private :

		UGraph graph;
        Gecode::IntArgs edges_source, edges_target;
        
        Gecode::IntVarArray path;

        //True : source->target, false : target->source
        Gecode::BoolVarArray edges_direction;

};

#endif //EULERIAN_PATH_HPP