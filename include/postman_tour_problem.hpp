/*!
 * \file postman_tour_problem.hpp
 * \author Antoine Richard
 */

#ifndef POSTMAN_TOUR_PROBLEM_HPP
#define POSTMAN_TOUR_PROBLEM_HPP

#include <iostream>
#include <stack>

#include <gecode/int.hh>
#include <gecode/search.hh>

#include <boost_graph.hpp>

/*!
 * \class PostmanTourProblem
 * \brief Instance of the postman tour problem.
 */
class PostmanTourProblem : public Gecode::Space {

    public :

        PostmanTourProblem(UGraph);

        PostmanTourProblem(bool, PostmanTourProblem&);
        
        virtual Gecode::Space* copy(bool);

        int getWeight(void) const;

        void print(void) const;

        UGraph toBoostGraph(void) const;

    protected :

        UGraph graph;
        Gecode::IntVarArray mult;
        Gecode::IntVar weightPath;
};

#endif //POSTMAN_TOUR_PROBLEM_HPP
