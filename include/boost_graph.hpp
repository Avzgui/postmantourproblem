/*!
 * \file boost_graph.hpp
 * \author Antoine Richard
 */

#ifndef BOOST_GRAPH_HPP
#define BOOST_GRAPH_HPP

#include <boost/graph/adjacency_list.hpp>

//Vertices properties
struct Vertex {
    int id;
    Vertex() : id(0) {}
    Vertex(int ID) : id(ID) {}
};

//Edges properties
struct Edge {
    int weight;
    std::string color;
    Edge() : weight(0), color("black") {}
   	Edge(int w) : weight(w), color("black") {}
   	Edge(int w, std::string c) : weight(w), color(c) {}
};

//Undirected graphs properties
typedef boost::property<boost::graph_name_t, std::string> graph_p;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
							 Vertex, Edge, graph_p> UGraph;

typedef boost::graph_traits<UGraph>::vertex_descriptor ugraph_vertex_t;
typedef boost::graph_traits<UGraph>::edge_descriptor ugraph_edge_t;

typedef boost::graph_traits<UGraph>::vertex_iterator ugraph_vertex_it;
typedef boost::graph_traits<UGraph>::edge_iterator ugraph_edge_it;

//directed graphs properties
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
							 Vertex, Edge, graph_p> DGraph;

typedef boost::graph_traits<DGraph>::vertex_descriptor dgraph_vertex_t;
typedef boost::graph_traits<DGraph>::edge_descriptor dgraph_edge_t;

typedef boost::graph_traits<DGraph>::vertex_iterator dgraph_vertex_it;
typedef boost::graph_traits<DGraph>::edge_iterator dgraph_edge_it;

#endif //BOOST_GRAPH_HPP