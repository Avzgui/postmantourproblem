/*!
 * \file graph_generator.hpp
 * \author Antoine Richard
 */

#ifndef GRAPH_GENERATOR_HPP
#define GRAPH_GENERATOR_HPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include <gecode/int.hh>
#include <gecode/search.hh>

#include <boost_graph.hpp>

/*!
 * \class GraphGenerator
 * \brief Graph generator constraint-based.
 */
class GraphGenerator : public Gecode::Space {

	public :

		GraphGenerator(int, int);

		GraphGenerator(bool, GraphGenerator&);
        
        virtual Gecode::Space* copy(bool);

        void print(void) const;

        void toGraphviz(std::string) const;

        UGraph toBoostGraph() const;

    protected :

    	int n_nodes;
    	int m_edges;
    	Gecode::IntVarArray edges_begin;
    	Gecode::IntVarArray edges_end;
};

#endif //GRAPH_GENERATOR_HPP
