#include <eulerian_path.hpp>

using namespace Gecode;

EulerianPath::EulerianPath(UGraph g) 
	: graph(g),
	  edges_source(boost::num_edges(graph)),
	  edges_target(boost::num_edges(graph)),
	  path(*this, boost::num_edges(graph), 0, boost::num_edges(graph)-1),
	  edges_direction(*this, boost::num_edges(graph), 0, 1)
{
	
	//First, each IntVar in path must be distinct
	distinct(*this, path);

	//Get all the edges' sources and target
	std::pair<ugraph_edge_it, ugraph_edge_it> e_it = boost::edges(graph);
    int i = 0;
    for( ; e_it.first != e_it.second; ++e_it.first){
       edges_source[i] = boost::source(*e_it.first, graph);
       edges_target[i] = boost::target(*e_it.first, graph);
       i++;
    }

	//For each edges of the path
	for(i = 0 ; i < (int) boost::num_edges(graph) ; i++){

		int j = (i+1)%boost::num_edges(graph);

		IntVar i_source(*this, 0, boost::num_vertices(graph)-1);
		IntVar i_target(*this, 0, boost::num_vertices(graph)-1);
		IntVar j_source(*this, 0, boost::num_vertices(graph)-1);
		IntVar j_target(*this, 0, boost::num_vertices(graph)-1);

		//get the sources
		element(*this, edges_source, path[i], i_source);
		element(*this, edges_source, path[j], j_source);

		//get the targets
		element(*this, edges_target, path[i], i_target);
		element(*this, edges_target, path[j], j_target);


		//Get element 1
		IntVar e1(*this, 0, boost::num_vertices(graph)-1);
		ite(*this, edges_direction[i], i_target, i_source, e1);

		//Get element 2
		IntVar e2(*this, 0, boost::num_vertices(graph)-1);
		ite(*this, edges_direction[j], j_source, j_target, e2);

		//Element 1 and 2 must be equal
		rel(*this, e1, IRT_EQ, e2);
	}

	branch(*this, path, INT_VAR_SIZE_MIN(), INT_VAL_MIN());
	branch(*this, edges_direction, INT_VAR_SIZE_MIN(), INT_VAL_MIN());
}

EulerianPath::EulerianPath(bool share, EulerianPath& ep) 
        : Space(share, ep), graph(ep.graph),
          edges_source(boost::num_edges(graph)),
	  	  edges_target(boost::num_edges(graph)) {
    
    for(int i = 0 ; i < (int) boost::num_edges(graph) ; i++){
    	edges_source[i] = ep.edges_source[i];
    	edges_target[i] = ep.edges_target[i];
    }
    
    path.update(*this, share, ep.path);
    edges_direction.update(*this, share, ep.edges_direction);

}

Space* EulerianPath::copy(bool share) {
    return new EulerianPath(share, *this);
}

void EulerianPath::print(void) const {
	for(int i = 0 ; i < (int) boost::num_edges(graph) ; i++){
		if(edges_direction[i].val()){
			std::cout << edges_source[path[i].val()] << " --> ";
			if(i == (int) boost::num_edges(graph)-1)
				std::cout << edges_target[path[i].val()] << std::endl;
		}
		else{
			std::cout << edges_target[path[i].val()] << " --> ";
			if(i == (int) boost::num_edges(graph)-1)
				std::cout << edges_source[path[i].val()] << std::endl;
		}
	}
}