#include <graph_generator.hpp>

using namespace Gecode;

GraphGenerator::GraphGenerator(int n, int m) 
	: n_nodes(n), m_edges(m), 
	  edges_begin(*this, m_edges, 0, n_nodes-1), 
	  edges_end(*this, m_edges, 0, n_nodes-1) {

	//end != begin for edges
	for(int i = 0 ; i < m_edges ; i++){
		rel(*this, edges_begin[i], IRT_NQ, edges_end[i]);
		//No parallel
		for(int j = 0 ; j < m_edges ; j++){
			if(j != i){
				//if not(begin1 == begin2 and end1 == end2)
				BoolVar b1(*this, 0, 1), b2(*this, 0, 1);
				rel(*this, edges_begin[i], IRT_EQ, edges_begin[j], b1);
				rel(*this, edges_end[i], IRT_EQ, edges_end[j], b2);
				rel(*this, b1, BOT_AND, b2, 0);

				//if not(begin1 == end2 and end1 == begin2)
				BoolVar b3(*this, 0, 1), b4(*this, 0, 1);
				rel(*this, edges_begin[i], IRT_EQ, edges_end[j], b3);
				rel(*this, edges_end[i], IRT_EQ, edges_begin[j], b4);
				rel(*this, b3, BOT_AND, b4, 0);
			}
		}
	}

	//* create a tree
	for(int i = 0 ; i < n_nodes-1 ; i++){
		IntVar next(*this, i+1, n_nodes);
		rel(*this, edges_begin[i], IRT_EQ, i);
		rel(*this, edges_end[i], IRT_EQ, next);
	}
	//*/

	unsigned int seed = rand() % 1000;
    Rnd r(seed);
	branch(*this, edges_begin, INT_VAR_SIZE_MIN(), INT_VAL_RND(r));
	branch(*this, edges_end, INT_VAR_SIZE_MIN(), INT_VAL_RND(r));
}

GraphGenerator::GraphGenerator(bool share, GraphGenerator& gg) 
		: Space(share, gg), n_nodes(gg.n_nodes), m_edges(gg.m_edges) {
    edges_begin.update(*this, share, gg.edges_begin);
    edges_end.update(*this, share, gg.edges_end);
}

Space* GraphGenerator::copy(bool share) {
    return new GraphGenerator(share, *this);
}

void GraphGenerator::print(void) const {
    std::cout << std::endl;
    std::cout << "begins : " << edges_begin << std::endl;
    std::cout << "ends : " << edges_end << std::endl;
    std::cout << std::endl;
}

void GraphGenerator::toGraphviz(std::string file_name) const {

	std::ofstream file;
	file.open(file_name);

	file << "graph G {" << std::endl;
	for(int i = 0 ; i < m_edges ; i++)
		file << "\t" << edges_begin[i] << " -- " << edges_end[i] 
			<< " [label=" << rand()%9 + 1 << ", color=\"black\"];" << std::endl;
	file << "}";

	file.flush();
	file.close();
}

UGraph GraphGenerator::toBoostGraph() const {
	UGraph g(0);

	//Add vertices
	ugraph_vertex_t* v = new ugraph_vertex_t[n_nodes];
	for(int i = 0 ; i < n_nodes ; i++)
		v[i] = boost::add_vertex(Vertex(i), g);

	//Add edges
	for(int i = 0 ; i < m_edges ; i++)
		boost::add_edge(v[edges_begin[i].val()], v[edges_end[i].val()], 
						Edge(rand()%9 + 1), g);

	return g;
}
