#include <postman_tour_problem.hpp>

using namespace Gecode;

PostmanTourProblem::PostmanTourProblem(UGraph g) 
    : graph(g), mult(*this, boost::num_edges(graph), 1, 2) {
             
    //Weight of the path contraint
    IntArgs weights(boost::num_edges(graph));
    int totWeight = 0;
    std::pair<ugraph_edge_it, ugraph_edge_it> e_it = boost::edges(graph);
    int i = 0;
    
    for( ; e_it.first != e_it.second; ++e_it.first){
       Edge e = get(boost::edge_bundle, graph)[*e_it.first];
       weights[i] = e.weight;
       totWeight += e.weight;
       i++;
    }

    weightPath = IntVar(*this, totWeight, totWeight*2);
    linear(*this, weights, mult, IRT_EQ, weightPath);

    //Degrees constraints
    IntVar m(*this, 2, 2);
    IntVar peer(*this, 0, 0);

    std::pair<ugraph_vertex_it, ugraph_vertex_it> v_it = boost::vertices(graph);
    for( ; v_it.first != v_it.second; ++v_it.first){
        int d = 0;
        std::stack<int> s;

        e_it = boost::edges(graph);
        i = 0;
        for( ; e_it.first != e_it.second; ++e_it.first){
            if(boost::target(*e_it.first, graph) == *v_it.first
            || boost::source(*e_it.first, graph) == *v_it.first){
                d++;
                s.push(i);
            }
            i++;
        }

        IntVar degree(*this, d, d*2);
        IntVarArgs dd(d);
        for(i = 0 ; i < d ; i++){
            dd[i] = mult[s.top()];
            s.pop();
        }

        linear(*this, dd, IRT_EQ, degree);
        mod(*this, degree, m, peer);
    }
    
    //Minimize the path's weight
    branch(*this, weightPath, INT_VAL_MIN());
    branch(*this, mult, INT_VAR_SIZE_MIN(), INT_VAL_MIN());
}

PostmanTourProblem::PostmanTourProblem(bool share, PostmanTourProblem& p) 
        : Space(share, p), graph(p.graph) {
    mult.update(*this, share, p.mult);
    weightPath.update(*this, share, p.weightPath);
}

Space* PostmanTourProblem::copy(bool share) {
    return new PostmanTourProblem(share, *this);
}

int PostmanTourProblem::getWeight(void) const {
    return weightPath.val();
}

void PostmanTourProblem::print(void) const {
    std::cout << std::endl;
    
    std::cout << "Weight min of the path : " << weightPath << std::endl;
    
    int i = 0;
    std::pair<ugraph_edge_it, ugraph_edge_it> e_it = boost::edges(graph);
    for( ; e_it.first != e_it.second; ++e_it.first){
        std::cout << boost::source(*e_it.first, graph) 
            << "--" << boost::target(*e_it.first, graph) 
            << "\t x" << mult[i] << std::endl;
        i++;
    }

    std::cout << std::endl;
}

UGraph PostmanTourProblem::toBoostGraph(void) const{
    UGraph cpy(graph);

    int i = 0;
    std::pair<ugraph_edge_it, ugraph_edge_it> e_it = boost::edges(graph);
    for( ; e_it.first != e_it.second; ++e_it.first){
        if(mult[i].val() == 2){
            ugraph_vertex_t source = boost::source(*e_it.first, cpy);
            ugraph_vertex_t target = boost::target(*e_it.first, cpy);
            boost::add_edge(source, target,
                        Edge(get(boost::edge_bundle, cpy)[*e_it.first].weight, "red"),
                        cpy);
        }
        i++;
    }

    return cpy;
}
