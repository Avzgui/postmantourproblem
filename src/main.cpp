/*!
 * \file main.cpp
 * \author Antoine Richard
 * \brief Main file of the postman tour project.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <stack>
#include <sys/time.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>

#include <postman_tour_problem.hpp>
#include <eulerian_path.hpp>
#include <graph_generator.hpp>

using namespace Gecode;
using namespace boost;


// main function
int main(int argc, char* argv[]) {

    if(argc < 3)
        return EXIT_FAILURE;

    srand(time(NULL)); //Initialize rand, just in case

    //A stack of Graph for the search
    std::stack<UGraph> graphs;

    //Check input
    std::string arg = argv[1];
    if(arg.compare("-I") == 0){ //Read the input file
        UGraph g;
        dynamic_properties dp;
        dp.property("node_id", get(&Vertex::id,  g));
        dp.property("label", get(&Edge::weight, g));
        dp.property("color", get(&Edge::color, g));
        std::ifstream file(argv[2]);

        read_graphviz(file, g, dp);
        graphs.push(g);
    }
    else if(arg.compare("-G") == 0){ //Generate
        if(argc == 4){ //Just one graph
            GraphGenerator* gg = new GraphGenerator(atoi(argv[2]), atoi(argv[3]));
            BAB<GraphGenerator> bab1(gg);
            delete gg;
            gg = bab1.next();

            if(gg == NULL)
                return EXIT_FAILURE;

            UGraph g;
            g = gg->toBoostGraph();
            graphs.push(g);
            delete gg;
        }
        else{ //Several graph with n vertices
            int n = atoi(argv[2]);
            for(int m = n ; m <= (n*(n-1)) / 2 ; m++){
                GraphGenerator* gg = new GraphGenerator(n, m);
                BAB<GraphGenerator> bab1(gg);
                delete gg;
                gg = bab1.next();

                if(gg == NULL)
                    return EXIT_FAILURE;

                UGraph g;
                g = gg->toBoostGraph();
                graphs.push(g);
                delete gg;
            }
        }
    }

    //Var for time checking
    struct timespec tbegin,tend;

    //For each graph in the stack
    int i = 0; // current graph ID
    while(!graphs.empty()){
        //Get the top graph
        UGraph g;
        g = graphs.top();
        i++;

        //Delete the top graph
        graphs.pop();

        std::cout << "---------- Graph " << i << "----------\n\n" << std::flush;

        /* ----- Step 1 : Graph Transformation ----- */

        //Space and Engine initialisation
        PostmanTourProblem* p = new PostmanTourProblem(g);
        BAB<PostmanTourProblem> bab2(p);
        delete p;

        //Get time
        long texec_transfo=0;
        clock_gettime(CLOCK_MONOTONIC, &tbegin);
        
        //Search first solution
        p = bab2.next();
        
        //Get time and execution time
        clock_gettime(CLOCK_MONOTONIC, &tend);
        texec_transfo=((long)(1000000000*(tend.tv_sec-tbegin.tv_sec)+((tend.tv_nsec-tbegin.tv_nsec))));
        
        if(p != NULL){

            /* ----- Step 2 : Eulerian Path Search ----- */

            //Get the transformed graph
            UGraph g2 = p->toBoostGraph();

            //Space and Engine initialisation
            EulerianPath* ep = new EulerianPath(g2);
            BAB<EulerianPath> bab3(ep);
            delete ep;

            //Get time
            long texec_path=0;
            clock_gettime(CLOCK_MONOTONIC, &tbegin);

            //Search first solution
            ep = bab3.next();

            //Get time and execution time
            clock_gettime(CLOCK_MONOTONIC, &tend);
            texec_path=((long)(1000000000*(tend.tv_sec-tbegin.tv_sec)+((tend.tv_nsec-tbegin.tv_nsec))));

            if(ep != NULL){
                //Output the solution founded
                std::cout << "Path founded (weight = " <<  p->getWeight() << ") : "<< std::endl;
                ep->print();

                std::cout << "\nGraph Transformation Time : " << texec_transfo << " ns";

                std::cout << "\nEulerian Path Search Time : " << texec_path << " ns";

                std::cout << "\nTotal Search Time : " << texec_transfo + texec_path << " ns\n\n";
            }

            delete ep;
        }

        delete p;
    }

    return EXIT_SUCCESS;
}

