# Postman Tour Problem #

## Introduction ##

The Postman Tour Problem is a search of a path, on a pondered and undirected graph,
which passes through each edges at least once.

This project's goal is to resolve this problem using constraint programming.

## Requirements ##

This project was realised with :

* Gecode
* Boost::Graph

You will need those librairies if you want to execute this project.